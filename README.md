### Translate APP

How to run application

1. Open node-api folder.
2. Install packages:
    ```
    npm install
    ```

3. Run server
    ```
    npm start
    ```
This will run server on `3010` port

4. Open client folder
5. Install packages:
    ```
    npm install
    ```
6. Run client app
    ```
    npm start
    ```
This will run app on `3000` port
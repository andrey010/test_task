"use strict"

const MyStorage = require('../storage/storage')

class MyController {
  
  async getText(req, res, next ) {
    try {
      const data = await MyStorage.getStorage()
      res.status(200).json(data)
    } catch(err) {
      next(err)
    }
    
  }

  async postText(req, res, next) {
    try {
      const id = +req.params.id
      const text = req.body.text
      await MyStorage.addItem(text, id)
      res.status(200).json({ message: 'Text saved'})
    } catch(err) {
      next(err)
    }
    
  }
}

module.exports = new MyController()
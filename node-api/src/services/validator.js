"use strict"

class MyValidator {
  
  buildError(code, message) {
    throw { code, message }
  };

  saveTextValidator = async (req, res, next) => {
    const text = req.body.text
    try {
      if (!text) {
        this.buildError(400, 'Text field cannot be empty !')
      }
      if (typeof text !== 'string') {
        this.buildError(400, 'Text should be a string !')
      }
      next()
    } catch(err) {
      next(err)
    }

  }
}

module.exports = new MyValidator()
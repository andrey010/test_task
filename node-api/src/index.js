"use strict"

const express = require('express');
const bodyParser = require('body-parser');
const myRouter = require('../src/routes/routes');
const { errorHandler, corsMiddleware } = require('./middlewares/middlewares');

const app = express();

app.use(bodyParser.json({ limit: "5mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));

app.use(corsMiddleware);

app.use('/api/v1/', myRouter);

app.use(errorHandler);

app.listen(3010, () => console.log('Example app is listening on port 3010.'));
"use strict"
const fs = require('fs')
const util = require('util')

class MyStorage {

  readFile = util.promisify(fs.readFile)
  writeFile = util.promisify(fs.writeFile)
  filePath = __dirname + '/text.json'

  getStorage = async () => {
    const data = await this.readFile(this.filePath, 'utf-8')
    const res = JSON.parse(data)

    return res
  }

  addItem = async (text, id) => {
    const data = await this.readFile(this.filePath, 'utf-8')
    let obj = JSON.parse(data)
    const checkArr = obj.items.filter(i => i.id === id)
    if (checkArr.length ===0) {
      throw {
        code: 404,
        message: 'Not found'
      }
    }
    const updated = obj.items.map(item => {
      if (item.id === id) {
        item.translation = text
      } 
      return item
    })
    obj.items = updated
    const json = JSON.stringify(obj)
    await this.writeFile(this.filePath, json, 'utf8')
    console.log('\x1b[36m%s\x1b[0m',`'INSERT "${text}" TO storage'`)    
  }

}

module.exports = new MyStorage()
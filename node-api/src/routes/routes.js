"use strict"

const express = require('express');
const MyController = require('../controllers/controllers');
const MyValidator = require('../services/validator')
const router = express.Router();

router.get('/get_text', MyController.getText)
router.post('/save_text/:id', MyValidator.saveTextValidator, MyController.postText)

module.exports = router;
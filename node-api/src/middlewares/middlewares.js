const errorHandler = (error, req, res, next) => {
  console.log('ERROR',error);
  const status = error.code || 500;
  const message = error.message || 'Some error occurred';
  res.status(status).json({
    message
  });
}

const corsMiddleware = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
}

module.exports = {
  errorHandler,
  corsMiddleware
}
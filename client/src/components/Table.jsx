import React , { useEffect, useState }from 'react'
import { TableRow } from './TableRow'
import { getText, saveText } from '../axios/axios'

const Table = (props) => {

  const [data, setData] = useState(null)

  const setActive = (id) => {
    const newArr = data.map((item) => {
      if (item.id === id) {
        item.active = true
      } else {
        item.active = false
      }
      return item
    })
    setData(newArr)
  }  

  useEffect(() => {
    (async () => {
      const forTranslate = await getText()
      const newData = forTranslate.data.map(item => {        
        return {
          ...item,
          active: false
        }
      })
      console.log(newData)
      setData(newData)
    })()       
  }, [])


  const handleSend = (e) => {
    e.stopPropagation()
    return async (text, id) => {
      console.log(text, id)
      const res = await saveText(text, id)
      console.log(res)
    }
  } 

  const handleChange = (e) => {
    return (id) => {
      const newArr = data.map((item) => {
        if (item.id === id) {
          item.translation = e.target.value
        }
        return item
      })
      setData(newArr)
    }
  }


  return (
    <table cellSpacing="0">
      <tbody>
        {data && 
          data.map((item, idx) => {
            console.log(item.translation)
            return (
              <TableRow 
                key={item.id}
                onSend={(e) => {
                  const save = handleSend(e)
                  save(item.translation, item.id)
                }}
                forTranslate={item.text}
                translated={item.translation}
                active={item.active}
                handleSetActive={() => setActive(item.id)}
                count={++idx}
                changeForTanslate={handleChange}
                changeTranslation={(e) => {
                  const setTranslate = handleChange(e)
                  setTranslate(item.id)
                }}
              />
            )
          })
        }
      </tbody>
    </table>
  )
}

export { Table }
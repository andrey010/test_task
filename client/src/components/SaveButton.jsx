import saveicon from '../img/saveicon.svg'
import dropdownarrow from '../img/dropdownarrow.svg'

const SaveButton = ({
  onClick
}) => {
  return(
    <div className="controls__save--button">
    <a href="#" onClick={(e) => onClick(e)}>
      <img src={saveicon} alt="saveicon" /> 
    Save
    </a>
    <a href="#" className="droppable__controls">
      <img src={dropdownarrow} alt="" />
    </a>
  </div>
  )
}

export { SaveButton }
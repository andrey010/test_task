import React from 'react'
import backicon from '../img/backicon.svg'
import { DroppableBtn } from './DroppableBtn'

const LeftToolTip = React.memo(() => {
  return (
    <div className="left__tooltip">
      <div className="back__tooltip">
        <a href="#"><span><img src={backicon} alt="backicon" /></span> Job #111111</a>
      </div>
      <div className="tooltip__menu--small">
        <a href="#">
          <span></span>
          <span></span>
          <span></span>
        </a>
      </div>
      <div className="tooltip__menu">
        <ul>
          <li className="droppable__menu">
            <DroppableBtn title="FILE" />
            {/* <div className="droppable__box">
              <ul>
                <li className="droppable__inner">
                  <a href="#">Package <span class="droppable__arrow"><img src="img/rightdrop.svg" alt="rightdrop" /></span></a>
                  <div className="droppable__sub">
                    <ul>
                      <li><a href="#">Download</a></li>
                      <li><a href="#">Upload</a></li>
                    </ul>
                  </div>
                </li>
                <li  className="droppable__inner">
                  <a href="#">XLIFF <span class="droppable__arrow"><img src="img/rightdrop.svg" alt="rightdrop" /></span></a>
                  <div class="droppable__sub" style="display:none;">
                    <ul>
                      <li><a href="#">Export</a></li>
                      <li><a href="#">Import</a></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <a href="#"><span class="logout__button"><img src="img/logouticon.svg" alt="logout" /></span> Logout</a>
                </li>
              </ul>
            </div> */}
          </li>
          <li className="droppable__menu">
          <DroppableBtn title="EDIT" />
           
            {/* <div className="droppable__box check__droppable">
              <ul>
                <li className="check__active">
                  <a href="#" data-check="search__field">
                    <input className="styled-checkbox" id="dcheck1" type="checkbox" value="search__input" />
                      <label for="dcheck1">Search</label>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <input className="styled-checkbox" id="dcheck2" type="checkbox" value="value1" />
                      <label for="dcheck2">Compact</label>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <input className="styled-checkbox" id="dcheck3" type="checkbox" value="value1" />
                      <label for="dcheck3">Live view</label>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <input className="styled-checkbox" id="dcheck4" type="checkbox" value="value1" />
                      <label for="dcheck4">Source text</label>
                  </a>
                </li>
              </ul>
            </div> */}
          </li>
          <li className="droppable__menu">
          <DroppableBtn title="VIEW " />
            
            {/* <div class="droppable__box segments__box">
                  <div class="el__drop--box">
                    <p>SEGMENTS</p>
                    <div class="check__drop">
                      <a href="#">
                        <span class="info__icon"><img src="img/segment1.svg" alt="segment" /></span>
                        Compact
                        <span class="key__shortcut">Alt+Y</span>
                      </a>

                      <a href="#">
                        <span class="info__icon"><img src="img/segment2.svg" alt="segment" /></span>
                        Source
                        <span class="key__shortcut">Alt+V</span>
                      </a>

                      <a href="#" class="droppable__view">
                        <img src="img/rightddrop.svg" alt="rightddrop" />
                        <span class="info__icon"><img src="img/segment3.svg" alt="segment" /></span>
                        Font size
                        <span class="key__shortcut">Alt+U</span>
                      </a>

                      <a href="#">
                        <span class="info__icon"><img src="img/segment4.svg" alt="segment" /></span>
                        Dark theme
                        <span class="key__shortcut">Alt+T</span>
                      </a>
                    </div>
                  </div>

                  <div class="el__drop--box">
                    <p>PREVIEW</p>
                    <div class="check__drop ">
                      <a href="#" class="check__active">
                        <span class="info__icon"><img src="img/preview1.svg" alt="segment" /></span>
                        Page
                        <span class="key__shortcut">Alt+Y</span>
                      </a>

                      <a href="#" class="check__active">
                        <span class="info__icon"><img src="img/preview2.svg" alt="segment" /></span>
                        Thumbnails
                        <span class="key__shortcut">Alt+V</span>
                      </a>

                      <a href="#" class="droppable__view">
                        <img src="img/rightddrop.svg" alt="rightddrop" />
                        <span class="info__icon"></span>
                        Origin
                      </a>

                      <a href="#" class="droppable__view">
                        <img src="img/rightddrop.svg" alt="rightddrop" />
                        <span class="info__icon"></span>
                        Position
                      </a>

                      <a href="#" class="droppable__view">
                        <img src="img/rightddrop.svg" alt="rightddrop" />
                        <span class="info__icon"></span>
                        Orientation
                      </a>
                    </div>
                  </div>
                </div> */}
          </li>
          <li>
          <DroppableBtn title="HELP" />
          </li>
        </ul>
      </div>
    </div>
  )
})

export { LeftToolTip }
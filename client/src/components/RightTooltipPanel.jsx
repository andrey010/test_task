

const RigthTooltipPanel = (props) => {

  return(
    <div className="tooltip__main--container right_panel">
			<div className="tooltip__main">
        {props.children}
			</div>	
		</div>
  )
}

export { RigthTooltipPanel }
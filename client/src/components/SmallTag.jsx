
const SmallTag = (props) => {
  const { icon, text, keys, type} = props
  return (
    <div className="small__tag">
      <a href="#">
        <img src={icon} alt={type} />
      </a>
      <div className="tag__float">
        <p>{text} <span>{keys}</span>
        </p>
      </div>
    </div>
  )
}

export { SmallTag }
import React from 'react'
import { BottomPart } from './BottomPart'
import { FullSidePart } from './FullSidePart'
import { RightPart } from './RightPart'

const TableRow = ({
  onSend,
  forTranslate,
  translated,
  count,
  active,
  handleSetActive,
  changeForTanslate,
  changeTranslation
}) => {

  return (
    <tr className={active ? 'active__translate' : ''} onClick={handleSetActive}>
      <td><div className="counter__tooltip"><p>{count}</p></div></td>
      <td>
        <div className="full__translation">
          <div className="full__top">
            <div className="full__top-left">
              <FullSidePart
                className="full__left--left"
                language='English - United Kingdom'
                text={forTranslate}
                onChange={changeForTanslate}
              />
              <FullSidePart
                className="full__left--right"
                language='Danish'
                text={translated}
                onChange={changeTranslation}
              />
            </div>
            <RightPart />
          </div>
          <BottomPart onSend={onSend} />                                                                                                                                      </div>
      </td>
    </tr>
  )
}

export { TableRow }
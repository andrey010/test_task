import iconsave from '../img/iconsave1.svg'
import doubleicon from '../img/doubleicon.svg'
import warnicon from '../img/warnicon.svg'
import commentnew from '../img/commentnew.svg'

const RightPart = () => {

  return (
    <div className="full__top--right">
      <div className="info__top--right">
        <div className="column__info">
          <div className="status__translate">
            <div className="status__info">
              <p>Confirmed</p>
            </div>
            <img src={iconsave} alt="confirmedicon" />
          </div>
          <div className="double__arrow">
            <a href="#" className="double__bottom">
              <img src={doubleicon} alt="doubleicon" />
            </a>
            <div className="double__placeholder">
              <p>2 repetitions</p>
            </div>
          </div>
        </div>
        <div className="column__info">
          <div className="status_col">
            <span><img src={warnicon} alt="warnicon" /></span>
          </div>
          <div className="status__info status__small">
            <p>MT</p>
            <div className="status__float">
              <p>Machine translation</p>
            </div>
          </div>
          <div className="status__comment">
            <a href="#"><img src={commentnew} alt="comment" /> <span>1</span></a>
          </div>
        </div>
      </div>
    </div>
  )
}

export { RightPart }
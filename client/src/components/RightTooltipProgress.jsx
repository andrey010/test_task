import updateIcon from '../img/updateicon.svg'

const RightTooltipProgress = (props) => {
  return (
    <div className="progress__tooltip">
      <div className="progress__inner">
        <p>Translation</p>
        <div className="progress__bar">
          <div className="active__progress" style={{width: '20%'}}></div>
        </div>
        <div className="progress__percent">
          <span>65%</span>
        </div>
        <div className="progress__words">
          <p>756/1365 words</p>
        </div>
        <div className="progress__update">
          <a href="#">
            <img src={updateIcon} alt="updateicon" />
          </a>
				</div>
      </div>
    </div>
  )
}

export { RightTooltipProgress }
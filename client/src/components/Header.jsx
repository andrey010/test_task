import React from 'react'
import { LeftToolTip } from './LeftTooltip'
import { RigthTooltip } from './RightTooltip'

const Header = React.memo(() => {

  return (
    <div className="tooltip__header">
      <LeftToolTip />
      <RigthTooltip />
  </div>
  )
})

export { Header }
import button1 from '../img/button1.svg'
import button2 from '../img/button2.svg'
import button3 from '../img/button3.svg'
import controls1 from '../img/controls1.svg'
import controls2 from '../img/controls2.svg'
import controls3 from '../img/controls3.svg'
import controls4 from '../img/controls4.svg'
import controls5 from '../img/controls5.svg'
import controls6 from '../img/controls6.svg'
import controlsdots from '../img/controlsdots.svg'
import { ElementError } from './ElementError'
import { SaveButton } from './SaveButton'
import { SmallTag } from './SmallTag'

const BottomPart = ({
  onSend
}) => {
  return (
    <div className="full__bottom">
      <div className="translate__controls">
        <div className="controls__left">
          <div className="controls__buttons--text">
            <SmallTag
              icon={button1}
              text='Bold text format'
              keys='Ctrl+B'
              type="button"
            />  
            <SmallTag
              icon={button2}
              text='Italic text format'
              keys='Ctrl+I'
              type="button"
            /> 
            <SmallTag
              icon={button3}
              text='Underline text format'
              keys='Ctrl+U'
              type="button"
            />
          </div>
          <div className="controls__small">
            <SmallTag
              icon={controls1}
              text='Remove formating'
              keys='F7'
            />
            <SmallTag
              icon={controls2}
              text='Insert tag'
              keys='F8'
            />
            <SmallTag
              icon={controls3}
              text='Copy source to target'
              keys='F10'
            />
            <SmallTag
              icon={controls4}
              text='Remove tags'
              keys='F^'
            />
            <SmallTag
              icon={controls5}
              text='Undo edit'
              keys='Ctrl + Z'
            /> 
            <SmallTag
              icon={controls6}
              text='Redo edit'
              keys='Ctrl+Shift+Z'
            />           
          </div>
          <div className="controls__more">
            <a href="#" data-preview="controls1">
              <img src={controlsdots} alt="controls" />
            </a>
          </div>
          <div className="controls__words">
            <p>69/1024</p>
          </div>
        </div>
        <div className="controls__right">

          <div className="controls__save">
            <SaveButton onClick={onSend} />
          </div>
          <div className="controls__errors">
            <ElementError error='2'/>
            <ElementError error='Missing formattings'/>
            <ElementError error='Identical text'/>
            <ElementError error='text info 3'/>
            <ElementError error='text info 20'/>
          </div>
        </div>
      </div>

      <div className="translate__details details__active">
        <a href="#">Show less</a>
      </div>
      <div className="translate__suggestions">
        <ul>
          <li><a href="#" data-switcher="suggestions__table">SUGGESTIONS <span>2</span></a></li>
          <li><a href="#" data-switcher="term__wrapper">TERM <span>2</span></a></li>
          <li><a href="#" data-switcher="history__wrapper">HISTORY <span>2</span></a></li>
          <li><a href="#" data-switcher="concordance__wrapper">CONCORDANCE</a></li>
          <li><a href="#" data-switcher="symbols__wrapper">SYMBOLS</a></li>
          <li><a href="#" data-switcher="qa__block">QA <span>17</span></a></li>
        </ul>
      </div>
    </div>
  )
}

export { BottomPart}
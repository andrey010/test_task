//import qouteicon from '../img/quoteicon.svg'
import React from 'react'
import ContentEditable from 'react-contenteditable'

const TranslationText = ({text, onChange}) => {

  return (
    <div className="translation__text">      
      <ContentEditable html={text} onChange={e => onChange(e)} />
    </div>
  )
}

export { TranslationText }
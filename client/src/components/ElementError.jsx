import warnicon from '../img/warnicon.svg'

const ElementError = ({ error }) => {
  return (
    <div className="elem__error">
      <p>
        <span>
          <img src={warnicon} alt="" />
        </span>
        {error}
      </p>
    </div>
  )
}

export { ElementError }
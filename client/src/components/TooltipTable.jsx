
const TooltipTable = (props) => {

  return(
    <div className="tooltip__table--wrapper">
      <div className="tooltip__table">
        {props.children}
      </div>
    </div>
  )
}

export { TooltipTable }
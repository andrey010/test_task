import { TranslationText } from "./TranslationText"
import elilipses from '../img/ellipses.svg'

const FullSidePart = ({
  className,
  language,
  text,
  onChange
}) => {

  return (
    <div className={className}>
      <div className="lang__info">
        <span>
          {language}
        </span>
        <div className="more__lang">
          <a href="#" data-more="more1" className="more__button"><img src={elilipses} alt="ellipses" /></a>
        </div>
      </div>

      <TranslationText
        text={text}
        onChange={onChange}
      />
    </div>
  )
}

export { FullSidePart }
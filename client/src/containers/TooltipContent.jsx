
const TooltipContent = (props) => {

  return(
    <div className="tooltip__content">
      <div className="splitter_panel tool__main--">
				{props.children}
      </div>
    </div>
  )
}

export { TooltipContent }
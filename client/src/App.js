import { Header } from './components/Header';
import { RigthTooltipPanel } from './components/RightTooltipPanel';
import { RightTooltipProgress } from './components/RightTooltipProgress';
import { Table } from './components/Table';
import { TooltipTable } from './components/TooltipTable';
import { TooltipContent } from './containers/TooltipContent';

function App() {
  return (
    <div className="tooltip__app">
      <Header />
      <TooltipContent>
        <RigthTooltipPanel>
          <RightTooltipProgress />
          <TooltipTable>
            <Table />
          </TooltipTable>
        </RigthTooltipPanel>
      </TooltipContent>
    </div>
  );
}

export default App;

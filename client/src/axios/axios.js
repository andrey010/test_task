import axios from 'axios'

const BASE_URL = 'http://127.0.0.1:3010/api/v1'

export const getText = async () => {
  const URL = `${BASE_URL}/get_text`
  let res = null
  try {
    const { data, status } = await axios.get(URL)
    res = { status, data: data.items }
  } catch (err) {
    res = {
      status: err.response.status,
      data: err.response.data
    }
  }
  return res
}

export const saveText = async (text, id) => {
  console.log({text})
  const URL = `${BASE_URL}/save_text/${id}`
  let res = null
  try {
    const { status, data } = await axios.post(URL, { text })
    res = { status, data }
  } catch (err) {
    res = {
      status: err.response.status,
      data: err.response.data
    }
  }
  
  return res
}

